import AllFilms from './components/AllFilms.vue';
import CreateFilm from './components/CreateFilm.vue';
import EditFilm from './components/EditFilm.vue';
 
export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllFilms
    },
    {
        name: 'create',
        path: '/create',
        component: CreateFilm
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditFilm
    }
];

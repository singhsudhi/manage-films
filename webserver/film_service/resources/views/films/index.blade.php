<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


            <div class="film">
                <div class="film-header">
                    {{ __('Films') }}
                    <span class="float-right"><a href="{{ route('films') }}" class="btn btn-sm btn-primary">New film</a></span>
                </div>

                <div class="film-body">

                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Release Date</th>
                            <th>Rating</th>
                            <th>Ticket Price</th>
                            <th>Country</th>
                            <th>Genre</th>
                            <th>Photo</th>
                          </tr>
                        </thead>
                        <tbody>

                          @foreach ($films as $film)
                          <tr>
                            <td>{{ $film->name }}</td>
                            <td>{{ $film->Description }}</td>
                            <td>{{ $film->release_date }}</td>
                            <td>{{ $film->rating }}</td>
                            <td>{{ $film->ticket_price }}</td>
                            <td>{{ $film->country }}</td>
                            <td>{{ $film->genre }}</td>
                            <td>{{ $film->photo }}</td>
                          </tr>    
                          @endforeach  

      
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="app">
<example-component />
</div>
<script src="{{ mix('/js/app.js') }}"></script>

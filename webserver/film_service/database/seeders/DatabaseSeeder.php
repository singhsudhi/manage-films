<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('films')->delete();

        DB::table('films')->insert([
            [
                'id'                       => 1,
                'name'                   => 'film1',
                'description'     => "this is film1 description",
                'release_date'                  => $now,
                'rating'        => 5,
                'ticket_price'      => 700,
                'country'    => 'India',
                'genre'                   => 'science fiction',
                'photo' => 'http://cdn.abc.com/photo1',
		'status' => 'Active',
                'created_at'   => $now,
                'updated_at'   => $now,
            ],
            [
                'id'                       => 2,
                'name'                   => 'film2',
                'description'     => "this is film2 description",
                'release_date'                  => $now,
                'rating'        => 4,
                'ticket_price'      => 700,
                'country'    => 'India',
                'genre'                   => 'science fiction',
                'photo' => 'http://cdn.abc.com/photo2',
		'status' => 'Active',
                'created_at'   => $now,
                'updated_at'   => $now,
            ],
            [
                'id'                       => 3,
                'name'                   => 'film3',
                'description'     => "this is film3 description",
                'release_date'                  => $now,
                'rating'        => 5,
                'ticket_price'      => 700,
                'country'    => 'India',
                'genre'                   => 'mystery',
                'photo' => 'http://cdn.abc.com/photo3',
		'status' => 'Active',
                'created_at'   => $now,
                'updated_at'   => $now,
            ],
        ]);

    }
}

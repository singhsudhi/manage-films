<?php

namespace App\Http\Controllers;

use App\Models\Films;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class FilmsController extends Controller
{
   
	public function index()
	    {
		$films = Films::all()->toArray();
		return array_reverse($films);      
	    }

	    public function store(Request $request)
	    {

		//echo '<pre>';print_r($request);exit;
		$film = new Films([
		    'name' => $request->input('name'),
		    'description' => $request->input('description'),
		    'release_date' => $request->input('release_date'),
		    'rating' => $request->input('rating'),
		    'ticket_price' => $request->input('ticket_price'),
		    'country' => $request->input('country'),
		    'genre' => $request->input('genre'),
		    'photo' => $request->input('photo'),
		    'status' => 'D'
		]);
		$film->save();

		return response()->json('Films created!');
	    }

	    public function show($id)
	    {
		$film = Films::find($id);
		return response()->json($film);
	    }

	    public function update($id, Request $request)
	    {
		$film = Films::find($id);
		$film->update($request->all());

		return response()->json('Films updated!');
	    }

	    public function destroy($id)
	    {
		$film = Films::find($id);
		$film->delete();

		return response()->json('Films deleted!');
   	}


}


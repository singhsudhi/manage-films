<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Films extends Model
{

	public $table = 'films';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'release_date', 'rating', 'ticket_price', 'country', 'genre', 'photo', 'status', 'created_at', 'updated_at'
    ];

}

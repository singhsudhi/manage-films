<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManageFilm extends Model
{

	public $table = 'manage_film';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'release_date', 'rating', 'ticket_price', 'country', 'genre', 'photo', 'status', 'created_at', 'updated_at'
    ];

}
